package com.mars.project;

public enum Enums {
	East(1,1,"E"),
	West (3,-1,"W"),
	North (0,1,"N"),
	South (2,-1,"S"),
	Left("L"),
	Right("R");
	
	int intDirection;
	int directionMark;
	String directionStr;
	String directionName;

	Enums(int intDirection, int directionMark, String directionStr) {
		this.directionMark = directionMark;
		this.intDirection = intDirection;
		this.directionStr = directionStr;
	}
	
	Enums(String directionName) {
		this.directionName = directionName;
	}
	
	


	public int getIntDirection() {
		return intDirection;
	}





	public void setIntDirection(int intDirection) {
		this.intDirection = intDirection;
	}





	public int getDirectionMark() {
		return directionMark;
	}





	public void setDirectionMark(int directionMark) {
		this.directionMark = directionMark;
	}





	public String getDirectionStr() {
		return directionStr;
	}





	public void setDirectionStr(String directionStr) {
		this.directionStr = directionStr;
	}



	public String getDirectionName() {
		return directionName;
	}

	public void setDirectionName(String directionName) {
		this.directionName = directionName;
	}

	public static int stringToValue(String val) throws Exception {
		
		for (Enums enm : values()) {
			if(enm.getDirectionStr().equals(val))
				return enm.getIntDirection();
		}
		throw new Exception("Değer bulunamadı");
	}

	public static int markStringToValue(String val) throws Exception {
		
		for (Enums enm : values()) {
			if(enm.getDirectionStr().equals(val))
				return enm.getDirectionMark();
		}
		throw new Exception("Değer bulunamadı");
	}

	public static String intToStr(int val) throws Exception {
		
		for (Enums enm : values()) {
			if(enm.getIntDirection() == val)
				return enm.getDirectionStr();
		}
		throw new Exception("Değer bulunamadı");
	}
}
