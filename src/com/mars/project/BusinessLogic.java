package com.mars.project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BusinessLogic {
	
	
	
	public static Map<String,Integer> CheckAndParseAreaValue(String input) throws Exception {
		Map<String, Integer> result = new HashMap<>();
		
		try {
			String arr[] = input.split(" ");
			result.put("x", Integer.parseInt(arr[0]));
			result.put("y", Integer.parseInt(arr[1]));
		}catch (Exception e) {
			throw new Exception("Area value parse error!");
		}
		
		return result;
	}
	
	
	public static RoverModel CheckAndParseStartPositionValue(String input,Map<String,Integer> areaValues) throws Exception {
		RoverModel result = new RoverModel();
		
		try {
			String arr[] = input.split(" ");
			
			if(arr.length != 3) throw new Exception(); 
			int x = Integer.parseInt(arr[0]);
			int y = Integer.parseInt(arr[1]);
			
			if(x > areaValues.get("x") || x < 0) throw new Exception(); 
			if(y > areaValues.get("y") || y < 0) throw new Exception(); 
			
			result.setCurrentPositionY(y);
			result.setCurrentPositionX(x);
			result.setCurrentDirection(arr[2]);
		}catch (Exception e) {
			throw new Exception("start position value error!");
		}
		
		return result;
	}
	
	
	
	public static List<MoveModel> CheckAndParseMoveData(String input) throws Exception {
		List<MoveModel> result = new ArrayList<>();
		
		try {
			List<String> arr = Split(input);
			
			if(arr.size() == 0) throw new Exception(); 
			
			for (String string : arr) {
				MoveModel mdl = new MoveModel();
				mdl.setMove(Arrays.asList(string.split("")));
				result.add(mdl);
			}
			
		}catch (Exception e) {
			throw new Exception("move data error!");
		}
		
		return result;
	}
	
	public synchronized static void Move(RoverModel rover, List<MoveModel> moveData,Map<String, Integer> areaValue) throws Exception {
		
		for (MoveModel moveModel : moveData) {
			String drc = GetDirection(moveModel.getMove(),rover.getCurrentDirection());
			rover.setCurrentDirection(drc);
			if(drc.equals(Enums.East.getDirectionStr()) || drc.equals(Enums.West.getDirectionStr())) {
				int xTemp = rover.getCurrentPositionX() + Enums.markStringToValue(drc);
				if(xTemp < 0 || areaValue.get("x") < xTemp) {
					System.out.println("Rover harita dışına hareket edemez lütfen sınırlar içerisinde olacak şekilde hareket seti giriniz.");
					throw new Exception("map limit error");
				}
				rover.setCurrentPositionX(xTemp);
			}else if(drc.equals(Enums.North.getDirectionStr()) || drc.equals(Enums.South.getDirectionStr())) {
				int yTemp = rover.getCurrentPositionY() + Enums.markStringToValue(drc);
				if(yTemp < 0 || areaValue.get("y") < yTemp) {
					System.out.println("Rover harita dışına hareket edemez lütfen sınırlar içerisinde olacak şekilde hareket seti giriniz.");
					throw new Exception("map limit error");
				}
				rover.setCurrentPositionY(yTemp);
			}
		}
		
		System.out.print(rover.getCurrentPositionX() + " " + rover.getCurrentPositionY() + " "  +rover.getCurrentDirection());
		System.out.println();
	}
	
	public static String GetDirection(List<String> data,String CurrentDirection) throws Exception {
		String result = "";
		String finalDirecton ="";
		
		int left = 0;
		int rigth = 0;
		
		for (String str : data) {
			if(str.equalsIgnoreCase(Enums.Left.getDirectionName())) {
				left++;
			}else if(str.equalsIgnoreCase(Enums.Right.getDirectionName())) {
				rigth++;
			}
		}
		
		finalDirecton = left > rigth ? Enums.Left.getDirectionName() : Enums.Right.getDirectionName();
		left -= rigth;
		
		result = CalcDirection(CurrentDirection,left,finalDirecton);
		
		
		
		return result;
	}
	
	
	public static String CalcDirection(String currentDirection, int count,String finalDirection) throws Exception {
		if(count == 0) return  currentDirection;
		if(count < 0) count = count * -1;
		String result = "";
		
		int currentDirVal = Enums.stringToValue(currentDirection); 
		
		if(finalDirection.equals(Enums.Left.getDirectionName())) {
			currentDirVal -= count % 4;
		}else if(finalDirection.equals(Enums.Right.getDirectionName())) {
			currentDirVal += count % 4;
		}
		
		currentDirVal = currentDirVal % 4;
		
		if(currentDirVal < 0) currentDirVal += 4;
		
		
		result = Enums.intToStr(currentDirVal);
		return result;
		
	}
	
	
	
	private static List<String> Split(String input) {
		List<String> result = new ArrayList<String>();
		String temp = "";
		for (int i = 0; i < input.length(); i++) {
			
			if(!Character.toString(input.charAt(i)).equalsIgnoreCase("M")) {
				temp += input.charAt(i);
			}else {
				result.add(temp);
				temp = "";
			}
		}
		return result;
	}

}
