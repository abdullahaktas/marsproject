package com.mars.project;

public class RoverModel {
	
	private int currentPositionX;
	private int currentPositionY;
	private String currentDirection;
	
	
	public int getCurrentPositionX() {
		return currentPositionX;
	}
	public void setCurrentPositionX(int currentPositionX) {
		this.currentPositionX = currentPositionX;
	}
	public int getCurrentPositionY() {
		return currentPositionY;
	}
	public void setCurrentPositionY(int currentPositionY) {
		this.currentPositionY = currentPositionY;
	}
	public String getCurrentDirection() {
		return currentDirection;
	}
	public void setCurrentDirection(String currentDirection) {
		this.currentDirection = currentDirection;
	}
	
	
	

}
