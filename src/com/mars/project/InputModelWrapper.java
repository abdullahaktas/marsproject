package com.mars.project;

import java.util.List;

public class InputModelWrapper {
	private RoverModel roverModel;
	private List<MoveModel> moveModel;
	
	
	public RoverModel getRoverModel() {
		return roverModel;
	}
	public void setRoverModel(RoverModel roverModel) {
		this.roverModel = roverModel;
	}
	public List<MoveModel> getMoveModel() {
		return moveModel;
	}
	public void setMoveModel(List<MoveModel> moveModel) {
		this.moveModel = moveModel;
	}
	
	
	
	
}
