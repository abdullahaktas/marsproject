package com.mars.project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainClass {
	
	

	public static void main(String[] args) throws Exception {
		Map<String, Integer> areaLimit = new HashMap<>();
		List<InputModelWrapper> modelWrapper = new ArrayList<InputModelWrapper>();
		String continues = "";
		
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Alan limiti giriniz: ");
		String xAndY = scanner.nextLine();
		
		areaLimit = BusinessLogic.CheckAndParseAreaValue(xAndY);
		
		while (!continues.equalsIgnoreCase("exit")) {
			RoverModel roverModel;
			List<MoveModel> moveData;
			InputModelWrapper mdl = new InputModelWrapper(); 
			
			System.out.println("Başlangıç pozisyonu giriniz: ");
			String firstPosition = scanner.nextLine();
			
			roverModel = BusinessLogic.CheckAndParseStartPositionValue(firstPosition, areaLimit);
			
			System.out.println("Hareket setini giriniz");
			String data = scanner.nextLine();
			moveData = BusinessLogic.CheckAndParseMoveData(data);
			
			
			mdl.setMoveModel(moveData);
			mdl.setRoverModel(roverModel);
			
			modelWrapper.add(mdl);
			System.out.println("devam etmek için entere basınız, çıkmak için exit yazıp entere basınız");
			continues = scanner.nextLine();
		}
		
		
		for (InputModelWrapper inputModelWrapper : modelWrapper) {
			BusinessLogic.Move(inputModelWrapper.getRoverModel(), inputModelWrapper.getMoveModel(),areaLimit);
		}
		
		
		
		
		
		
		

	}

}
