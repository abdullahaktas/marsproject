import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.mars.project.BusinessLogic;
import com.mars.project.MoveModel;
import com.mars.project.RoverModel;

class Tests {

	@Test
	void testAreaValue() {
		String input = "5 5";
		Map<String, Integer> response = new HashMap<>();
		try {
			response = BusinessLogic.CheckAndParseAreaValue(input);
		} catch (Exception e) {
			fail("exception : " + e.getMessage());
		}
		
		assertEquals(5, response.get("x"));
		assertEquals(5, response.get("y"));
		
	}
	
	
	@Test
	void testStartPositionValue() {
		String input = "1 2 N";
		Map<String, Integer> areaValues = new HashMap<>();
		RoverModel response = null;
		areaValues.put("x", 5);
		areaValues.put("y", 5);
		
		try {
			response = BusinessLogic.CheckAndParseStartPositionValue(input, areaValues);
		} catch (Exception e) {
			fail("exception : " + e.getMessage());
		}
		
		assertEquals(1, response.getCurrentPositionX());
		assertEquals(2, response.getCurrentPositionY());
		assertEquals("N", response.getCurrentDirection());
		
	}
	
	
	
	@Test
	void testParseMoveData() {
		String input = "LLMRRMMRM";
		List<MoveModel> response = null ;
		List<String> moveData = new ArrayList<>();
		
		moveData.add("L");
		moveData.add("L");
		try {
			response = BusinessLogic.CheckAndParseMoveData(input);
		} catch (Exception e) {
			fail("exception : " + e.getMessage());
		}
		
		assertEquals(4, response.size());
		assertEquals(moveData, response.get(0).getMove());
		
	}

	
	
	@Test
	void testGetDirection() {
		String response = "";
		List<String> move = new ArrayList<String>();
		move.add("L");
		
		
		try {
			response = BusinessLogic.GetDirection(move, "N");
		} catch (Exception e) {
			fail("exception : " + e.getMessage());
		}
		assertEquals("W", response);
		
	}

	
	@Test
	void testCalcDirection() {
		String response = "";
		
		
		try {
			response = BusinessLogic.CalcDirection("N", -1, "L");
		} catch (Exception e) {
			fail("exception : " + e.getMessage());
		}
		assertEquals("W", response);
		
	}

}
